import { Injectable, Logger } from '@nestjs/common';
import { SecretManagerServiceClient } from '@google-cloud/secret-manager';
import { Environment } from 'src/core/types/environment.type';

require('dotenv').config();

@Injectable()
export class AppSettingsService {
  private readonly secretClient = new SecretManagerServiceClient();
  private readonly logger = new Logger(AppSettingsService.name);
  getEnvironment(): Environment {
    return this.getSetting('env') as Environment;
  }

  getSetting(envName: string) {
    const variable = process.env[envName];
    if (!variable) {
      this.logger.error(`Could not find env variable ${envName}`);
    }
    return variable;
  }

  async getSecret(secretName: string) {
    if (process.env.env === 'local') {
      this.logger.debug(`Resolving secret from local .env file: ${secretName}`);
      return Promise.resolve(process.env[secretName]);
    }
    try {
      const [version] = await this.secretClient.accessSecretVersion({
        name: `projects/${
          process.env.project_id
        }/secrets/${this.getEnvironment()}-${secretName}/versions/latest`,
      });
      this.logger.debug(
        `Successfully extracted secret <${secretName}>(${this.getEnvironment()}) from google secret manager`,
      );
      return version.payload.data.toString();
    } catch (err) {
      this.logger.error(
        `Failed to load secret: <${secretName}>(${this.getEnvironment()})`,
        err,
      );
      return Promise.resolve(null);
    }
  }
}
