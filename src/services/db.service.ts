import { Injectable, Logger } from '@nestjs/common';
import { resolve } from 'path';
import { ConnectionOptions } from 'typeorm';
import { AppSettingsService } from './app-settings.service';

@Injectable()
export class DatabaseService {
  private readonly logger = new Logger(DatabaseService.name);
  constructor(private readonly settings: AppSettingsService) {}
  private env: string;
  private options: ConnectionOptions = null;

  async init(): Promise<ConnectionOptions> {
    this.env = this.settings.getSetting('env');
    this.logger.debug('Initializing ORM');
    this.options = {
      type: 'mysql',
      host: this.settings.getSetting('sql_address'),
      port: Number(this.settings.getSetting('sql_port')),
      logging:
        (this.settings.getSetting('sql_logging').split(',') as any) || 'all',
      username: this.settings.getSetting('sql_user'),
      password: await this.settings.getSecret('sql_password'),
      database: this.settings.getSetting('sql_database'),
      synchronize: Boolean(this.settings.getSetting('sql_sync')),
      entities: [
        resolve(__dirname + '/../entity/**/*.ts'),
        resolve(__dirname + '/../entity/**/*.js'),
      ],
      migrations: [
        __dirname + '/migration/*.js',
        __dirname + '/migration/*.ts',
      ],
      subscribers: [
        __dirname + '/subscriber/*.js',
        __dirname + '/subscriber/*.ts',
      ],
      cli: {
        entitiesDir: 'dist/**/*.js',
        migrationsDir: '../migration',
        subscribersDir: '../subscriber',
      },
    };
    if (this.env !== 'local') {
      const extras = {
        socketPath: `/cloudsql/${process.env.DB_SOCKET_PATH}`,
      };
      const dbPwd: string = await this.settings.getSecret('sql_password');
      (this.options as any).extra = extras;
      (this.options as any).password = dbPwd;
    }
    const loggedOptions = { ...this.options };
    (loggedOptions as any).password = 'REMOVED';
    this.logger.debug(`Connection info ${loggedOptions}`);
    return this.options;
  }
}
