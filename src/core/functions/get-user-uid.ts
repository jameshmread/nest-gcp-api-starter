import { Request } from 'express';
import { getDecodedToken } from './get-token';

export async function getRequestingUserId(request: Request) {
  const token = await getDecodedToken(request);
  return token.uid;
}
