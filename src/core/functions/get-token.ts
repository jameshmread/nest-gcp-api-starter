import * as admin from 'firebase-admin';
import { Request } from 'express';
import { UnauthorizedException } from '@nestjs/common';

export async function getDecodedToken(request: Request) {
  try {
    const token = request.headers['authorization'].split(' ')[1];
    return await admin.auth().verifyIdToken(token);
  } catch (error) {
    throw new UnauthorizedException('Token verify error', error);
  }
}
