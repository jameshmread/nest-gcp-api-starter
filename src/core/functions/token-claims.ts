import admin from 'firebase-admin';

export function getPermissions(
  token: admin.auth.DecodedIdToken,
): Array<string> {
  return token.permissions;
}
export function getRoles(token: admin.auth.DecodedIdToken): Array<string> {
  return token.roles;
}
