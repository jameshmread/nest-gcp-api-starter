import { Injectable, NestMiddleware, Logger } from '@nestjs/common';
import { NextFunction, Request, Response } from 'express';

@Injectable()
export class LoggingInterceptor implements NestMiddleware {
  private logger = new Logger('HTTP');

  use(request: Request, response: Response, next: NextFunction): void {
    const { ip, method } = request;
    const now = Date.now();

    response.on('close', () => {
      const { statusCode } = response;
      if (statusCode < 400) {
        this.logger.log(
          `${method} ${request.path} | ${statusCode} | ${Date.now() - now}ms`,
        );
      } else {
        this.logger.error(
          `${method} ${request.path} | ${statusCode} | ${ip} | ${Date.now() -
            now}ms`,
        );
      }
    });

    next();
  }
}
