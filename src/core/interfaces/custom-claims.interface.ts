export interface ICustomClaims {
  roles: Array<string>;
  permissions: Array<string>;
}
