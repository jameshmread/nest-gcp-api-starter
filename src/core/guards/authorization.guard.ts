import {
  Injectable,
  CanActivate,
  ExecutionContext,
} from '@nestjs/common';
import { Request } from 'express';
import { Observable } from 'rxjs';
import { getDecodedToken } from '../functions/get-token';

@Injectable()
export class AuthGuard implements CanActivate {
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const request: Request = context.switchToHttp().getRequest();
    return getDecodedToken(request).then(token => {
      return token !== void 0;
    });
  }
}
