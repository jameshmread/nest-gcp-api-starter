import {
  Injectable,
  CanActivate,
  ExecutionContext,
  ForbiddenException,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Request } from 'express';
import { getDecodedToken } from '../functions/get-token';
import { getRoles } from '../functions/token-claims';

@Injectable()
export class RoleGuard implements CanActivate {
  constructor(private reflector: Reflector) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const requiredRoles = this.reflector.get<string[]>(
      'roles',
      context.getHandler(),
    );
    if (!requiredRoles || requiredRoles.length === 0) {
      return true;
    }
    const request: Request = context.switchToHttp().getRequest();
    const token = await getDecodedToken(request);
    const claimedRoles = getRoles(token);
    if (!claimedRoles) {
      throw new ForbiddenException(
        `You have no roles assigned to you, so cannot use this resource.`,
      );
    }
    const isAllowed =
      claimedRoles.filter(role => requiredRoles.includes(role)).length > 0;
    if (!isAllowed) {
      throw new ForbiddenException(
        `Incorrect roles given, this requires: ${requiredRoles.toString()}`,
      );
    } else {
      return true;
    }
  }
}
