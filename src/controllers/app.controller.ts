import { Controller, Get, UseGuards } from '@nestjs/common';
import { Roles } from 'src/core/decorators/role.decorator';
import { AuthGuard } from 'src/core/guards/authorization.guard';
import { AppService } from '../services/app.service';

@Controller('app')
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  publicGet(): string {
    return this.appService.getHello();
  }
  @Get('auth')
  @UseGuards(AuthGuard)
  @Roles('required-role')
  restrictedGet(): string {
    return this.appService.getHello();
  }
}
