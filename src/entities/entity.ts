import { CreateDateColumn, Entity, UpdateDateColumn } from "typeorm";

@Entity("TestEntities")
export class TestEntity {
      @CreateDateColumn()
      createdAt?: Date;
      @UpdateDateColumn()
      updatedAt?: Date;
}
