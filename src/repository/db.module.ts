import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TestEntity } from 'src/entities/entity';
import { AppSettingsService } from '../services/app-settings.service';
import { DatabaseService } from '../services/db.service';

@Module({
  providers: [DatabaseService, AppSettingsService],
  exports: [DatabaseService, AppSettingsService, TypeOrmModule],
  imports: [
    TypeOrmModule.forRootAsync({
      useFactory: (ormService: DatabaseService) => ormService.init(),
      inject: [DatabaseService],
      imports: [DatabaseModule],
    }),
    TypeOrmModule.forFeature([
      TestEntity
    ]),
  ],
})
export class DatabaseModule {}
