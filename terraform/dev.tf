terraform {
  required_version = ">= 0.13.5"
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "3.5.0"
    }
    random = {
      source  = "hashicorp/random"
      version = "3.1.0"

    }
  }
}
provider "google" {
  credentials = file(var.credentials_file)
}


resource "google_sql_database_instance" "my_sql" {
  name = "${var.env}-my_sql"
  database_version = var.db_version
  region           = var.region
  project          = var.project_name

  settings {
    tier = "db-f1-micro"
    location_preference {
      zone = var.zone
    }
    maintenance_window {
      day  = "7" # sunday
      hour = "3" # 3am
    }
    ip_configuration {
      ipv4_enabled = "true"
      authorized_networks {
        value = var.db_instance_access_cidr
      }
    }
  }
}

resource "google_sql_database" "my_sql_db" {
  name     = "${var.env}-my_sql"
  project  = var.project_name
  instance = var.sql_instance_name
}

# create user
resource "random_id" "user_password" {
  byte_length = 8
}
resource "google_sql_user" "my_sql_user" {
  name     = var.db_user_name
  project  = var.project_name
  instance = var.sql_instance_name
  password = random_id.user_password.hex
}

#https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/app_engine_application
#App Engine applications cannot be deleted once they're created; you have to delete the entire project to delete the application. Terraform will report the application has been successfully deleted; this is a limitation of Terraform, and will go away in the future. Terraform is not able to delete App Engine applications.
resource "google_app_engine_application" "app" {
  project     = var.project_id
  location_id = var.app-engine-region-group
}

resource "google_app_engine_standard_app_version" "app-engine-instance" {
      depends_on = [ google_sql_database_instance.my_sql ]
  project    = var.project_name
  runtime    = "nodejs12"

  entrypoint {
    shell = "node ./dist/main.js"
  }
  deployment {
    zip {
      source_url = "https://storage.googleapis.com/${google_storage_bucket.bucket.name}/${google_storage_bucket_object.object.name}"
    }
  }
  env_variables = {
    sql_address                = google_sql_database_instance.my_sql.ip_address.0.ip_address
    DB_SOCKET_PATH             = google_sql_database_instance.my_sql.connection_name
    sql_database               = google_sql_database.my_sql_db.name
    sql_user                   = var.db_user_name
    sql_sync                   = false
    sql_logging                = "query,error,warn"
    sql_port                   = 3306
    env                        = var.env
  }

  delete_service_on_destroy = true
}

resource "google_storage_bucket" "bucket" {
  project = var.project_name
  name    = "default-app-engine-${var.project_name}"
}

resource "google_storage_bucket_object" "object" {
  name   = "default-app-engine"
  bucket = google_storage_bucket.bucket.name
  source = var.app_engine_storage_bucket_source
}
