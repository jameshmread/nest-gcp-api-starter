variable db_instance_access_cidr {
  description = "The IPv4 CIDR to provide access the database instance. Defaults to any"
  default     = "0.0.0.0/0"
}

variable "credentials_file" {
  type    = string
  default = "./gcp-service-account-key.json"
}

variable "region" {
  default = "us-central1"
}
variable "app-engine-region-group" {
  default = "us-central"
}

variable "zone" {
  default = "us-central1-c"
}
variable "env" {
  type = string
  description = "The environment we are currently in"
  default = "dev"
}

variable "project_name" {
  type    = string
  default = ""
}
variable "project_id" {
  type    = number
  default = 0000000000000
}
variable "db_version" {
  type    = string
  default = "MYSQL_8_0"
}
variable "db_user_name" {
  type    = string
  default = "developer"
}
variable "sql_instance_name" {
  type        = string
  description = "The name of the MYSQL instance we can put a database into"
  default     = "developer"
}

variable "app_engine_storage_bucket_source" {
  type        = string
  description = "The file location we upload into the storage bucket"
  default     = "../dist.zip"
}
