# Template for api with resources

A starter that provides infrastructure code using Terraform.

This should be ready for both a dev and local environment use.

Reason for making this was I have needed an api for a few projects and having to copy over things from other projects is time consuming and frustrating. Let along the annoying infrastructure issues which always seem to waste a day in setting up a hosted dev environment that works with env vars, secrets etc. with a working CI/CD deployment pipeline.

You always end up needing these if the project is to scale. So why not bake it in for free.

- Nestjs
- TypeORM
- CI/CD Gitlab
- Terraform
- Hosted on Google Cloud Platform

